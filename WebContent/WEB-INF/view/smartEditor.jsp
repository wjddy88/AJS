<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="/resources/smartEditor/js/HuskyEZCreator.js" type="text/javascript"></script>
</head>

<body>
	
	<textarea id="smarteditor"></textarea>
	
	<script type="text/javascript">
		var editor_object = [];
	
	    nhn.husky.EZCreator.createInIFrame({
	        oAppRef: editor_object,
	        elPlaceHolder: "smarteditor",
	        sSkinURI: "/resources/smartEditor/SmartEditor2Skin.html", 
	        htParams : {
	            bUseToolbar : true,             
	            bUseVerticalResizer : true,     
	            bUseModeChanger : true, 
	        }
	    });
	
	</script>
</body>
</html>