<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	a {
		text-decoration: none;
		color: black;;
	}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/view/admin/common/top.jsp"></jsp:include>
<hr>
<h2>FAQ관리</h2>
<button type="button" onclick="goWriteForm()">등록</button>
<table border="1">
	<tr align="center">
		<td>순번</td>
		<td>제목</td>
		<td>내용</td>
		<td>작성자</td>
		<td>조회수</td>
		<td>공개여부</td>
		<td>작성일</td>
	</tr>
	<c:forEach var="faq" begin="${paging.startContentsIdx}" end="${paging.endContentsIdx}" items="${FAQList}" step="1" varStatus="status">
		<tr>
			<td>${paging.totContentsCnt - status.index}</td>
			<td>${faq.title}</td>
			<td>${faq.contents}</td>
			<td>${faq.userId}</td>
			<td>${faq.hit}</td>
			<td>${faq.viewYn}</td>
			<td>${faq.regDtm}</td>
		</tr>
	</c:forEach>
</table>
<a href="javascript:goPage('1')">처음</a>
<c:if test="${paging.pageNo > 1}">
	<a href="javascript:goPage('${paging.beforePage}')">이전</a>
</c:if>
<c:if test="${paging.pageNo eq 1}">
	<a href="javascript:alert('첫 페이지입니다.')">이전</a>
</c:if>
<c:forEach var="i" begin="${paging.startPageByNowBlock}" end="${paging.endPageByNowBlock}" step="1">
	<c:if test="${i eq paging.pageNo}">
		<a style="color:blue" href="javascript:goPage('${i}')">${i}</a>
	</c:if>
	<c:if test="${i ne paging.pageNo}">
		<a href="javascript:goPage('${i}')">${i}</a>
	</c:if>	
</c:forEach>
<c:if test="${paging.pageNo < paging.totPageCnt }">
	<a href="javascript:goPage('${paging.nextPage}')">다음</a>
</c:if>
<c:if test="${paging.pageNo eq paging.totPageCnt }">
	<a href="javascript:alert('마지막 페이지입니다.')">다음</a>
</c:if>
<a href="javascript:goPage('${paging.totPageCnt}')">끝</a>
<script>
	var msg = '${msg}';
	
	if (msg == "writeSuccess"){
		alert('글이 작성되었습니다.')
	}
	
	function goPage(pageNo){
		location.href="faqList.do?pageNo=" + pageNo;
	}
	
	function goWriteForm(){
		location.href="faqWrite.do";
	}
</script>
</body>
</html>