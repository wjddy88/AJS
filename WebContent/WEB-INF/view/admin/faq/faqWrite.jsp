<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/WEB-INF/view/admin/common/top.jsp"></jsp:include>
<hr>
<form name="f">
	<div>
		<label for="viewYn">공개여부</label>
		<input type="checkbox" name="viewYn" value="Y" />
	</div>
	<div>
		<label for="title">제목</label>
		<input type="text" name="title" size="38" />
	</div>
	<div>
		<label for="contents">내용</label>
		<textarea name="contents" rows="8" cols="40"></textarea>
	</div>
	<div>
		<button type="button" onclick="doWrite()">등록</button>
		<button type="button" onclick="cancelWrite()">취소</button>
	</div>
</form>
<script>
	function doWrite(){
		
		var form1 = document.f;
		
		form1.action = "faqWriteAction.do";
		form1.method = "POST";
		form1.submit();
	
	}

	function cancelWrite(){
		location.href="faqList.do";
	}
</script>
</body>
</html>