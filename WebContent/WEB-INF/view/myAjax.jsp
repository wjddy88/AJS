<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>GET방식</h1>
	<input type="text" id="myInput" value="멍충이">
	<input type="button" value="데이터 가져오기" onclick="getDataByAjaxByGet()">
	
	<br/>
	<form id="f1" name="f1">
		<h1>POST방식</h1>
		<input type="text" name="input0" value="0">
		<input type="text" name="input1" value="1">
		<input type="text" name="input2" value="2">
		<input type="text" name="input3" value="3">
		<input type="text" name="input4" value="4">
		<input type="button" value="데이터 가져오기" onclick="getDataByAjaxByPost()">
	</form>
	<script type="text/javascript">
		function getDataByAjaxByGet() {
			
			var xhttp = new XMLHttpRequest();
			var myInputEl = document.getElementById("myInput");
			var url =  "/ajax/getAjax.do?data=" + encodeURI(myInputEl.value);
			
			xhttp.open("GET", url, true);
			
			xhttp.onerror = function(e) {
				console.error(e);
			};
			
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					alert(this.responseText.trim());
				}
			}
			
			xhttp.send(null);
			
		}
		
		function getDataByAjaxByPost() {
			
			var xhttp = new XMLHttpRequest();
			var myInputEl = document.getElementById("myInput");
			var url =  "/ajax/postAjax.do";
			var f1El = document.forms["f1"];
			var param = serialize(f1El);
			
			xhttp.open("POST", url , true);
			
			xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					alert(this.responseText.trim());
				}
			}
			
			xhttp.onerror = function(e) {
				console.error(e);
			};
			

			xhttp.send(param);

		}

		function serialize(form) {
			if (!form || form.nodeName !== "FORM") {
				return;
			}
			var i, j, q = [];
			for (i = form.elements.length - 1; i >= 0; i = i - 1) {
				if (form.elements[i].name === "") {
					continue;
				}
				switch (form.elements[i].nodeName) {
				case 'INPUT':
					switch (form.elements[i].type) {
					case 'text':
					case 'tel':
					case 'email':
					case 'hidden':
					case 'password':
					case 'button':
					case 'reset':
					case 'submit':
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						break;
					case 'checkbox':
					case 'radio':
						if (form.elements[i].checked) {
							q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						}
						break;
					}
					break;
				case 'file':
					break;
				case 'TEXTAREA':
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
					break;
				case 'SELECT':
					switch (form.elements[i].type) {
					case 'select-one':
						q.push(form.elements[i].name + "="
								+ encodeURIComponent(form.elements[i].value));
						break;
					case 'select-multiple':
						for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
							if (form.elements[i].options[j].selected) {
								q.push(form.elements[i].name+ "=" + encodeURIComponent(form.elements[i].options[j].value));
							}
						}
						break;
					}
					break;
				case 'BUTTON':
					switch (form.elements[i].type) {
					case 'reset':
					case 'submit':
					case 'button':
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						break;
					}
					break;
				}
			}
			return q.join("&");
		}
	</script>
	
</body>
</html>