package com.brain.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.brain.dto.UserDTO;
import com.brain.util.SettingFieldsByClassInfoUtil;

public class RdbmsDAO {
	
	SettingFieldsByClassInfoUtil sfcu = new SettingFieldsByClassInfoUtil();
	
	public Connection getConnection(){
		
		Connection con = null;
		
		try {
			con = ConnectionPool.getInstance().getConnection();
		} catch (SQLException e) {
			System.out.println("SQLException : RdbmsDAO.getConnection SQLException" );
		}
		
		return con;
	}
	public void releaseConnection(Connection con){
		try {
			ConnectionPool.getInstance().releaseConnection(con);
		} catch (SQLException e) {
			System.out.println("SQLException : RdbmsDAO.releaseConnection SQLException" );
		}
	}
	
	public int runNonQuery(String sql, ArrayList<String> parameters) {
		
	int result = 0;
	Connection conn = null;
	PreparedStatement pstmt = null;
	
	try {
		
		conn = this.getConnection();
		
		pstmt = conn.prepareStatement(sql);
		
		if (parameters != null) {
			
			for( int i = 0; i < parameters.size(); i++ ) {
				pstmt.setString(i+1, parameters.get(i));
			}
			
		}
		
		result = pstmt.executeUpdate();
		
	} catch (Exception ex) {
		ex.printStackTrace();
	} finally {
		releaseConnection(conn);
	}
	return result;
	}
	
	public ArrayList<Object> selectList(String sql, Class clazz ,ArrayList<String> parameters){
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		ArrayList<Object> list = null;
		
		try {
			
			conn = getConnection();
			
			pstmt = conn.prepareStatement(sql);
			
			if (parameters != null) {
				for( int i = 0; i < parameters.size(); i++ ) {
					pstmt.setString(i+1, parameters.get(i));
				}
			}
			rs = pstmt.executeQuery(sql);
			
			list =  this.sfcu.getObjectByClassInfo( clazz, rs );
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			releaseConnection(conn);
		}
		 
		return list;
		
	}
}
