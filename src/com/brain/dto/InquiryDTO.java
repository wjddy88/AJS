package com.brain.dto;

import java.util.ArrayList;

public class InquiryDTO {
	
	
	private int idx;
	
	private String title;
	private String contents;
	private String userNm;
	private String phone0;
	private String phone1;
	private String phone2;
	private String userId;
	private String email;
	private String answerType;
	private String regDtm;
	
	private ArrayList<AnswerDTO> answerList;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getPhone0() {
		return phone0;
	}
	public void setPhone0(String phone0) {
		this.phone0 = phone0;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAnswerType() {
		return answerType;
	}
	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}
	public String getRegDtm() {
		return regDtm;
	}
	public void setRegDtm(String regDtm) {
		this.regDtm = regDtm;
	}
	public ArrayList<AnswerDTO> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(ArrayList<AnswerDTO> answerList) {
		this.answerList = answerList;
	}

}
