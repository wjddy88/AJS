package com.brain.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brain.dto.UserDTO;

public class LoginCheckFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		
		UserDTO sUser = (UserDTO) req.getSession().getAttribute("sUser");
		
		if (sUser == null) {
			
			String reqURI = req.getRequestURI();
			
			if ( this.checkAuthPage(reqURI)) {
				filterChain.doFilter(request, response);
			} else {
				resp.sendRedirect("/login.do");
				return;
			}
		} else {
			filterChain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}
	
	private boolean checkAuthPage(String reqURI) {
		
		boolean result = false;
		
		String tempUrl = null;
		
		ArrayList<String> urlList = this.getAnonymousPermissionPageList();
		
		for(int i = 0; i < urlList.size(); i++) {
			
			tempUrl = urlList.get(i);
			
			if(reqURI.contains(tempUrl)) {
				result = true;
			}
			
		}
			
		return result;
	}
	
	private ArrayList<String> getAnonymousPermissionPageList(){
		
		ArrayList<String> urlList = new ArrayList<String>();
		
		urlList.add("/test");
		urlList.add("/index.jsp");
		urlList.add("/main.do");
		urlList.add("/login.do");
		urlList.add("/loginAction.do");
		
		return urlList;
	}

}
