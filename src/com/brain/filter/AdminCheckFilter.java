package com.brain.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brain.dto.UserDTO;

public class AdminCheckFilter extends LoginCheckFilter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		
		String requestURI = req.getRequestURI();
		
		UserDTO sUser = (UserDTO) req.getSession().getAttribute("sUser");
		
		if(requestURI.contains("/admin")) {
			
			if(sUser.getAdminYn().equals("Y")) {
				filterChain.doFilter(request, response);
			} else {
				resp.sendRedirect("/main.do");
			}
			
		} else {
			super.doFilter(request, response, filterChain);
		}
	}
}
