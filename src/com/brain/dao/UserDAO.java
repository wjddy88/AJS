package com.brain.dao;

import java.util.ArrayList;

import com.brain.dto.UserDTO;

public interface UserDAO {
	public ArrayList<UserDTO> selectUserByWhere(String where);
}
