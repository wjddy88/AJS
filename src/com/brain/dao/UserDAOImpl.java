package com.brain.dao;

import java.util.ArrayList;

import com.brain.dto.UserDTO;
import com.brain.jdbc.RdbmsDAO;
import com.brain.util.SettingFieldsByClassInfoUtil;

public class UserDAOImpl extends RdbmsDAO implements UserDAO  {

	SettingFieldsByClassInfoUtil sfcu = new SettingFieldsByClassInfoUtil();
	
	@Override
	public ArrayList<UserDTO> selectUserByWhere(String where) {
				
		String sql = "select * from tbUSer";
		
		return (ArrayList)selectList(sql, UserDTO.class, null);

	}
	
}
