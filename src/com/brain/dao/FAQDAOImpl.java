package com.brain.dao;

import java.util.ArrayList;

import com.brain.dto.FAQDTO;
import com.brain.jdbc.RdbmsDAO;
import com.brain.util.SettingFieldsByClassInfoUtil;

public class FAQDAOImpl extends RdbmsDAO implements FAQDAO {
	
	SettingFieldsByClassInfoUtil sfcu = new SettingFieldsByClassInfoUtil();
	
	@Override
	public ArrayList<FAQDTO> selectAllFAQList(){
		
		String sql = "select * from tbFAQ order by idx desc";
		
		return (ArrayList)selectList(sql, FAQDTO.class, null);
		
	}
	
	@Override
	public int writeFaq(FAQDTO faq) {
		
		String sql = "INSERT INTO ajs.tbFAQ (title,contents,userId, hit, viewYn, delYn,regDtm)";
		sql = sql + "VALUES (?, ?, ?, '0', ?, 'N', now())";
		
		ArrayList<String> parameters = new ArrayList<String>();
		parameters.add(0, faq.getTitle());
		parameters.add(1, faq.getContents());
		parameters.add(2, faq.getUserId());
		parameters.add(3, faq.getViewYn());
		
		return runNonQuery(sql, parameters);
	}
}
