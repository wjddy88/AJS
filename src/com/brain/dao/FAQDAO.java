package com.brain.dao;

import java.util.ArrayList;

import com.brain.dto.FAQDTO;

public interface FAQDAO {
	public ArrayList<FAQDTO> selectAllFAQList();
	public int writeFaq(FAQDTO faq);
}
