package com.brain.restController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.util.ControllerUtil;

@WebServlet("/ajax/postAjax.do")
public class mySamplePostAjaxController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	
	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/html");
		try {
			PrintWriter out = res.getWriter();
			
			out.print("input0 의 데이터는 " + req.getParameter("input0") + "\n");
			out.print("input1 의 데이터는 " + req.getParameter("input1") + "\n");
			out.print("input2 의 데이터는 " + req.getParameter("input2") + "\n");
			out.print("input3 의 데이터는 " + req.getParameter("input3") + "\n");
			out.print("input4 의 데이터는 " + req.getParameter("input4") + "\n");
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}