package com.brain.restController;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.util.ControllerUtil;

@WebServlet("/ajax/getAjax.do")
public class mySampleGetAjaxController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	
	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		getAjaxData(req, res, "myTestAjax");
	}
}