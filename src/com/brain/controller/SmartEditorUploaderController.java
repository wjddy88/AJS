package com.brain.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.UUID;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.util.ControllerUtil;


@WebServlet("/multiplePhotoUpload.do")
public class SmartEditorUploaderController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		try {
			
			int cnt = 0;
			
			String sFileInfo = null;
			String filename = req.getHeader("file-name");
			String filename_ext = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();
			
			String[] allow_file = { "jpg", "png", "bmp", "gif" };

			
			for (int i = 0; i < allow_file.length; i++) {
				if (filename_ext.equals(allow_file[i])) {
					cnt++;
				}
			}

			if (cnt == 0) {
				PrintWriter print = res.getWriter();
				print.print("NOTALLOW_" + filename);
				print.flush();
				print.close();
			} else {
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
				
				int numRead;
				
				String today = formatter.format(new java.util.Date());
				String realFileNm = today + UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
				String dftFilePath = req.getSession().getServletContext().getRealPath("/");
				String filePath = dftFilePath + "resources" + File.separator + "images" + File.separator + "smartEditor" + File.separator;
				String rlFileNm = filePath + realFileNm;
				
				File file = new File(filePath);
				
				if (!file.exists()) {
					file.mkdirs();
				}

				InputStream is = req.getInputStream();
				OutputStream os = new FileOutputStream(rlFileNm);
				
				PrintWriter print = res.getWriter();
				
				byte b[] = new byte[Integer.parseInt(req.getHeader("file-size"))];
				
				while ((numRead = is.read(b, 0, b.length)) != -1) {
					os.write(b, 0, numRead);
				}
				
				if (is != null) {
					is.close();
				}
				
				os.flush();
				os.close();

				sFileInfo = "&bNewLine=true";
				sFileInfo += "&sFileName=" + filename;
				sFileInfo += "&sFileURL=" + "/resources/images/smartEditor/" + realFileNm;
				
				print.print(sFileInfo);
				print.flush();
				print.close();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
