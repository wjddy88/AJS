package com.brain.controller;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import com.brain.dto.UserDTO;
import com.brain.service.UserServiceImpl;
import com.brain.util.ControllerUtil;

@WebServlet("/main.do")
public class MainController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	private UserServiceImpl us = new UserServiceImpl();
	
	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		String redirctUrl = "main";
		
		boolean isRedirect = false;
		
		if(session.getAttribute("sUser") == null) {
			
			UserDTO loginUser = new UserDTO();
			
			loginUser.setUserId("admin");
			loginUser.setUserNm("안종한");
			loginUser.setState("I");
			loginUser.setAdminYn("Y");
			
			session.setAttribute("sUser", loginUser);
			
		}
		
		ArrayList<UserDTO> userList = this.us.selectUserByWhere("");
		
		for (UserDTO userDTO : userList) {
			System.out.println(userDTO.getUserId());
		}
		
		fireRedirctOrForward(isRedirect, req, res, redirctUrl);
		
	}
	
}
