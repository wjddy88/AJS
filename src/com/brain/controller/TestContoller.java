package com.brain.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.util.ControllerUtil;

@WebServlet("/test/*")
public class TestContoller extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	
	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		String redirctUrl = null;
		
		boolean isRedirect = false;
		
		if(reqUrl.contains("smartEditorSmaple.do")) {
			redirctUrl = "smartEditor";
		} else if(reqUrl.contains("myAjax.do")) {
			redirctUrl = "myAjax";
		}
		
		fireRedirctOrForward(isRedirect, req, res, redirctUrl);
		
	}
	
}
