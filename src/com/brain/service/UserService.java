package com.brain.service;

import java.util.ArrayList;

import com.brain.dto.UserDTO;

public interface UserService {
	public ArrayList<UserDTO> selectUserByWhere(String where);
}
