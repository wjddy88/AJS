package com.brain.service;

import java.util.ArrayList;

import com.brain.dao.FAQDAOImpl;
import com.brain.dto.FAQDTO;

public class FAQServiceImpl implements FAQService {
	
	private FAQDAOImpl faqDAO = new FAQDAOImpl(); 
	
	@Override
	public ArrayList<FAQDTO> selectAllFAQList(){
		return faqDAO.selectAllFAQList();
	}
	
	@Override
	public int writeFaq(FAQDTO faq) {
		return faqDAO.writeFaq(faq);
	}
}
