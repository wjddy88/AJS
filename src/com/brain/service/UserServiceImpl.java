package com.brain.service;

import java.util.ArrayList;

import com.brain.dao.UserDAOImpl;
import com.brain.dto.UserDTO;

public class UserServiceImpl implements UserService {

	private UserDAOImpl userDAO = new UserDAOImpl();
	
	@Override
	public ArrayList<UserDTO> selectUserByWhere(String where) {
		return userDAO.selectUserByWhere(where);
	}

}
