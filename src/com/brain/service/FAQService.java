package com.brain.service;

import java.util.ArrayList;

import com.brain.dto.FAQDTO;

public interface FAQService {
	public ArrayList<FAQDTO> selectAllFAQList();
	public int writeFaq(FAQDTO faq);
}
