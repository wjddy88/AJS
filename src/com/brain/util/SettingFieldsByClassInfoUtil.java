package com.brain.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SettingFieldsByClassInfoUtil {

	public ArrayList<Object> getObjectByClassInfo( Class clazz, ResultSet rs ) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		
		ArrayList<Object> objList = new ArrayList<Object>();
		
		while( rs.next() ) { 
			objList.add( this.setObjectData( clazz, rs ) );
		}
		
		return objList;
		
	}
	
	private Object setObjectData( Class clazz, ResultSet rs ) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		
		String setterPrefix = "SET";
		
		Constructor<?> cons = clazz.getConstructors()[ 0 ];
		
		Field[] filedsArr = clazz.getDeclaredFields();
		
		Method[] dtoMethodArr = clazz.getMethods();
		
		Object tempObj = cons.newInstance();
		
		cons.setAccessible(true);
		
		for (Field tempField : filedsArr) {
			for (Method tempMethod : dtoMethodArr) {
				
				String methodNm = tempMethod.getName().toUpperCase();
				String fieldNm = tempField.getName().toUpperCase();
				
				if( methodNm.equals( setterPrefix + fieldNm ) ) {
					this.callSetters( tempField, tempMethod, tempObj, rs );
				}
			}
		}
		
		return tempObj;
	}
	
	private void callSetters( Field tempField, Method tempMethod, Object tempObj, ResultSet rs ) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException {
		
		String tempFieldType = tempField.getType().toString();
		
		if(tempFieldType.contains("String")) {
			tempMethod.invoke( tempObj, rs.getString(tempField.getName()) );
		} else if(tempFieldType.contains("int")) {
			tempMethod.invoke( tempObj, rs.getInt(tempField.getName()) );
		} else if(tempFieldType.contains("long")) {
			tempMethod.invoke( tempObj, rs.getLong(tempField.getName()) );
		} else if(tempFieldType.contains("double")) {
			tempMethod.invoke( tempObj, rs.getDouble(tempField.getName()) );
		} else if(tempFieldType.contains("float")) {
			tempMethod.invoke( tempObj, rs.getFloat(tempField.getName()) );
		} else if(tempFieldType.contains("BigDecimal")) {
			tempMethod.invoke( tempObj, rs.getBigDecimal(tempField.getName()) );
		} else if(tempFieldType.contains("beelen")) {
			tempMethod.invoke( tempObj, rs.getInt(tempField.getName()) );
		} else if(tempFieldType.contains("byte")) {
			tempMethod.invoke( tempObj, rs.getByte(tempField.getName()) );
		} else if(tempFieldType.contains("short")) {
			tempMethod.invoke( tempObj, rs.getShort(tempField.getName()) );
		} else if(tempFieldType.contains("Date")) {
			tempMethod.invoke( tempObj, rs.getDate(tempField.getName()) );
		}
	}
}
