package com.brain.util;

public class PagingUtil {
	
	private int pageNo;
	private int totContentsCnt;
	private int totPageCnt;
	private int startPageByNowBlock;
	private int endPageByNowBlock;
	private int startContentsIdx;
	private int endContentsIdx;
	private int beforePage;
	private int nextPage;
	
	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	
	public int getTotContentsCnt() {
		return totContentsCnt;
	}

	public void setTotContentsCnt(int totContentsCnt) {
		this.totContentsCnt = totContentsCnt;
	}

	public int getTotPageCnt() {
		return totPageCnt;
	}

	public void setTotPageCnt(int totPageCnt) {
		this.totPageCnt = totPageCnt;
	}

	public int getStartPageByNowBlock() {
		return startPageByNowBlock;
	}

	public void setStartPageByNowBlock(int startPageByNowBlock) {
		this.startPageByNowBlock = startPageByNowBlock;
	}

	public int getEndPageByNowBlock() {
		return endPageByNowBlock;
	}

	public void setEndPageByNowBlock(int endPageByNowBlock) {
		this.endPageByNowBlock = endPageByNowBlock;
	}

	public int getStartContentsIdx() {
		return startContentsIdx;
	}

	public void setStartContentsIdx(int startContentsIdx) {
		this.startContentsIdx = startContentsIdx;
	}

	public int getEndContentsIdx() {
		return endContentsIdx;
	}

	public void setEndContentsIdx(int endContentsIdx) {
		this.endContentsIdx = endContentsIdx;
	}
	
	public int getBeforePage() {
		return beforePage;
	}

	public void setBeforePage(int beforePage) {
		this.beforePage = beforePage;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public void setPaging(String pageNumber, int totContentsCnt, int rowCnt, int blockCnt) {
		
		int nowBlock = 0;
		
		try {
			this.pageNo = Integer.parseInt(pageNumber);
		} catch (Exception e) {
			this.pageNo = 1;
		} finally {
			nowBlock = this.pageNo / (blockCnt + 1);
		}

		this.startPageByNowBlock = (nowBlock * blockCnt) + 1;
		this.endPageByNowBlock = (nowBlock + 1) * blockCnt;
		this.startContentsIdx = ((this.pageNo - 1) * rowCnt);
		this.endContentsIdx = this.pageNo * rowCnt - 1;
		this.beforePage = this.pageNo - 1;
		this.nextPage = this.pageNo + 1;
		this.totContentsCnt = totContentsCnt;
		this.totPageCnt = (int) Math.ceil((double) this.totContentsCnt / rowCnt );
		
		if (this.endPageByNowBlock > this.totPageCnt) {
			this.endPageByNowBlock = this.totPageCnt;
		}
		
		if (this.endContentsIdx > this.totContentsCnt) {
			this.endContentsIdx = this.totContentsCnt;
		}
		
		System.out.println(nowBlock);
		System.out.println(startPageByNowBlock);
		System.out.println(endPageByNowBlock);
		System.out.println(startContentsIdx);
		System.out.println(endContentsIdx);
	}
}
