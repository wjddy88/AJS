package com.brain.util;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class ControllerUtil extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		doPost(req, res);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		String reqUrl = req.getRequestURL().toString();
		
		fireController( reqUrl, req, res, req.getSession());
		
	}
	
	protected void getAjaxData( HttpServletRequest req, HttpServletResponse res, String ajaxUrl ) {
		
		String prefix = "/WEB-INF/ajax/";
		String subfix = ".jsp";
		
		ajaxUrl = prefix + ajaxUrl + subfix;
		
		this.forwardToUrl(req, res, ajaxUrl);
	}
	
	protected void fireRedirctOrForward( boolean isRedirect, HttpServletRequest req, HttpServletResponse res, String viewUrl ) {
		
		String prefix = "/WEB-INF/view/";
		String subfix = ".jsp";
		
		if(isRedirect == false) {
			viewUrl = prefix + viewUrl + subfix;
		}
		
		if (isRedirect) {
			this.redirctToUrl(res, viewUrl);
		} else {
			this.forwardToUrl(req, res, viewUrl);
		}
	}
	
	private void redirctToUrl( HttpServletResponse res, String viewUrl ) {
		try {
			res.sendRedirect(viewUrl);
		} catch (IOException e) {
			System.out.println("redirctToUrl : "  + viewUrl + ", Date : " + new Date().getTime());
		}
	}
	
	private void forwardToUrl( HttpServletRequest req, HttpServletResponse res, String viewUrl ) {
		RequestDispatcher rd = req.getRequestDispatcher(viewUrl);
		try {
			rd.forward(req, res);
		} catch (ServletException | IOException e) {
			System.out.println("forwardToUrl : "  + viewUrl + ", Date : " + new Date().getTime());
		}
	}
	
	
	public abstract void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session );
	
}
