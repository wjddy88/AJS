package com.brain.adminController;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.util.ControllerUtil;

@WebServlet("/admin/inquiry/*")
public class AdminInquiryController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	
	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		String redirctUrl = "main";
		
		boolean isRedirect = false;
		
		fireRedirctOrForward(isRedirect, req, res, redirctUrl);
		
	}
	
}