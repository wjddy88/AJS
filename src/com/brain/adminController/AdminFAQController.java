package com.brain.adminController;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.brain.dto.FAQDTO;
import com.brain.service.FAQServiceImpl;
import com.brain.util.ControllerUtil;
import com.brain.util.PagingUtil;

@WebServlet("/admin/faq/*")
public class AdminFAQController extends ControllerUtil {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void fireController( String reqUrl, HttpServletRequest req, HttpServletResponse res, HttpSession session ) {
		
		String viewUrl = null;
		
		FAQServiceImpl fs = new FAQServiceImpl();
		
		boolean isRedirect = false;
		
		if(reqUrl.contains("/faqList.do")) {
			
			ArrayList<FAQDTO> faqList = fs.selectAllFAQList();
			PagingUtil paging = new PagingUtil();
			int totContentsCnt = faqList.size();
			final int rowCnt = 4;
			final int blockCnt = 5;
			
			String pageNo = req.getParameter("pageNo");
			
			if (pageNo == null || pageNo.length() < 1) {
				
				pageNo = "1";
				
			}
			
			paging.setPaging(pageNo, totContentsCnt, rowCnt, blockCnt);
			
			req.setAttribute("FAQList", faqList);
			req.setAttribute("paging", paging);
			
			viewUrl = "admin/faq/faqList";
			
		} else if(reqUrl.contains("/faqDetail.do")) {
			viewUrl = "admin/faq/faqDetail";
		} else if(reqUrl.contains("/faqWrite.do")) {
			viewUrl = "admin/faq/faqWrite";
		} else if(reqUrl.contains("/faqWriteAction.do")) {
			
			FAQDTO faq = new FAQDTO();
			
			int result = 0;
			
			String title = req.getParameter("title");
			String contents = req.getParameter("contents");
			String viewYn = req.getParameter("viewYn");
			
			faq.setTitle(title);
			faq.setContents(contents);
			faq.setViewYn(viewYn);
			//TODO 로그인기능 완료 후 세션아이디로 수정하기
			faq.setUserId("admin");
			
			result = fs.writeFaq(faq);
			
			if (result == 1) {
				
				req.setAttribute("msg", "writeSuccess");
				//TODO 리다이렉트? faq 등록 후 리스트 나오게 하기
				viewUrl = "admin/faq/faqList";
				
			} else {
				isRedirect = true;
				viewUrl = "main.do";
			}
			
		} else {
			isRedirect = true;
			viewUrl = "main.do";
		}
		
		fireRedirctOrForward( isRedirect, req, res, viewUrl );
		
	}
	
}
